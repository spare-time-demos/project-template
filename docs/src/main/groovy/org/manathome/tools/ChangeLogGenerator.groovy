// ChangeLogGenerator.groovy
// retrieve github issues for given project and generate changelog file.
// @author man@home

package org.manathome.tools

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

def milestone  = this.args[0] // unused
def projectUrl = this.args[1]
def projectId  = this.args[2]
def gitlabAccessToken  = this.args[3]
def changelogFileToGenerate = this.args[4]

println "  ChangeLogGenerator: generating changelog from gitlab: ${new Date().format("YYYY-MM-dd")}"
println "  ChangeLogGenerator: ${projectUrl}, to ${changelogFileToGenerate} "

def post     = new URL("https://gitlab.com/api/v4/projects/${projectId}/issues")
def jsonText = post.getText( requestProperties: ["Private-Token": "${gitlabAccessToken}"])
def json     = new JsonSlurper().parseText(jsonText)

new File(new File("${changelogFileToGenerate}").parent).mkdirs()
def changelogFile = new File("${changelogFileToGenerate}")

changelogFile.write   "= Changelog\n"
changelogFile.append  "man@home\n"
changelogFile.append  "${new Date().format("YYYY-MM-dd")}\n"
changelogFile.append  ":jbake-type:   page\n"
changelogFile.append  ":jbake-tags:   documentation\n"
changelogFile.append  ":jbake-status: published\n"
changelogFile.append  ":icons:        font\n\n"

changelogFile.append  "NOTE: this is a generated footnote:[generated at ${new Date().format("YYYY-MM-dd HH:mm")} based on gitlab.com issues by ChangelogGenerator.] changelog file for ${projectUrl}[].\n\n"
changelogFile.append "For current issue state see ${projectUrl}/boards[gitlab issues boards]\n\n"

def sortedJson = json.sort{i1, i2 -> return -1 * ("${i1?.milestone?.title ?: "0.0.0.0"}" <=> "${i2?.milestone?.title ?: "0.0.0.0"}")}

println JsonOutput.prettyPrint(JsonOutput.toJson(sortedJson))

def lastMilestone = null
sortedJson.each { issue ->

    if(lastMilestone?.id != issue.milestone?.id ||
       lastMilestone != null && issue.milestone == null) {
        if(issue.milestone == null) {
            changelogFile.append  "\n\n'''"
            changelogFile.append  "\n* _without_ a Milestone\n\n"
        } else {
            changelogFile.append  "\n\n'''"
            milestoneMarkup = "\n* ${projectUrl}/milestones/${issue.milestone?.iid}[*Milestone ${issue.milestone?.title}*] " +
                              "${issue.milestone?.description} " +
                              "${issue.milestone?.due_date}" +
                              "${issue.milestone?.state == "active" ? "icon:tags[role=\"green\"] active" : ""}" +
                              "\n\n"

            changelogFile.append milestoneMarkup
        }
    }
    lastMilestone = issue.milestone

    // link to issue https://gitlab.com/..../issues/40

    markupString = "\n ** _${issue.title}_, " +
            "icon:tags[role=\"${issue.state == "closed" ? "grey": "red"}\"]" +
            "*${issue.state}*, " +
            "${projectUrl}/issues/${issue.iid}[issue *${issue.iid}*], " +
            " ^${issue.labels.join(",")?.replaceAll("\\s","")}^ \n"

    changelogFile.append  markupString
}
changelogFile.append  "\n"
changelogFile.append  "\n'''"
changelogFile.append  "\n"

println "  ChangeLogGenerator: done."
