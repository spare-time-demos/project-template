<article class="post">
   <#include "header.ftl">
     
    <div id="content">
       ${post.body}
    </div>
	
    <footer>
    	<#include "../commons/footer-tags.ftl">	
    </footer>
</article>
