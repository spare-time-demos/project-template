<#include "header.ftl">
	
	<#include "menu.ftl">
	
	<div id="main">
		<ul class="posts">
                <header>
                    <h1>LADRS</h1>
                </header>
                <#list published_ladrs as post>
					<#if (last_month)??>
						<#if post.date?string("MMMM yyyy") != last_month>
							</ul>
							<h4>${post.date?string("MMMM yyyy")}</h4>
							<ul class="post">
						</#if>
					<#else>
						<h4>${post.date?string("MMMM yyyy")}</h4>
						<ul class="post">
					</#if>
					<li>
				        <article data-file="${content.rootpath}${post.noExtensionUri!post.uri}" data-target="article">
				        	<header>
				            	<p>
									${post.date?string("yyyy-MM-dd")} - 
									<a href="${content.rootpath}${post.noExtensionUri!post.uri}">
									<#escape x as x?xml>ladr-${post.id}: ${post.title}</#escape></a>  
									<#if ((config.site_includeReadTime!'true')?boolean == true)> <div class="eta"></div></#if>
								</p>
				            <header>
				        </article>
				    </li>
					<#assign last_month = post.date?string("MMMM yyyy")>
				</#list>
            </ul>
	</div>
		
	<#include "commons/sidebar.ftl">
<#include "footer.ftl">