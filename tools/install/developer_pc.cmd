rem install several used tools for this project
rem use with care, does updates on existing intallations!

rem @see    https://chocolatey.org 
rem @author man@home

rem java jdk v8
choco upgrade jdk8 

rem java ide
choco upgrade eclipse

rem git versioning tool with ui
choco upgrade git
choco upgrade gitextensions --pre 

rem db ui
choco upgrade dbeaver

rem prometheus monitoring server
choco upgrade prometheus

rem reverse proxy
choco upgrade traefik 

rem logging forwarder to elk stack
choco upgrade filebeat

rem log server (beginning elk stack)
choco upgrade elasticsearch
choco upgrade kibana
