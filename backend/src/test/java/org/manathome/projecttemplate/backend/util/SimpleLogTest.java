package org.manathome.projecttemplate.backend.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.manathome.projecttemplate.backend.BackendApplicationTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

public class SimpleLogTest {

  static final Logger logger = LoggerFactory.getLogger(BackendApplicationTests.class);

  @Test
  @DisplayName("logging done")
  public void writeInfoLog() {
      logger.info("one info test log message from writeLog {}", 5);
  }

  @Test
  @DisplayName("logging mdc can be set")
  public void writeLogWithMdc() {
      MDC.put("session", "123456");
      logger.debug("some debug test log message from with session mdc");
      String newMdc = MDC.get("session");
      assertThat(newMdc).as("set mdc").containsIgnoringCase("123456");
      MDC.remove("session");
      String noMdc = MDC.get("session");
      assertThat(noMdc).as("removed mdc").isNull();
      logger.debug("some debug test log message from without session mdc");
  }

}
