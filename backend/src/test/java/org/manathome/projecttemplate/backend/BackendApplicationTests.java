package org.manathome.projecttemplate.backend;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/** dummy unit test. */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class BackendApplicationTests {
  
  static final Logger logger = LoggerFactory.getLogger(BackendApplicationTests.class);

  @Test
  public void contextLoads() {
  }
  
  @Test
  public void writeLog() {
      logger.debug("some debug test log message from writeLog in spring boot context tests");
  }

}
