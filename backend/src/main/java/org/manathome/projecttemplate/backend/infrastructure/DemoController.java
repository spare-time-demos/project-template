package org.manathome.projecttemplate.backend.infrastructure;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.annotation.Timed;

/** sample spring demo controller.
 *  http://localhost:8080/demo/info
 * */
@Timed
@RestController
@RequestMapping("demo")
public class DemoController {

 private static final Logger logger = LoggerFactory.getLogger(DemoController.class);

 @GetMapping("info")
 public String info() {
   logger.trace("DemoController.info called.");
   return "a demo info: " + LocalDate.now();
 }

}
