package org.manathome.projecttemplate.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** starting spring boot web application stand alone.
 *  
 * @author man@home
 * @since  2018-10-01
 * */
@SpringBootApplication
public class BackendApplication {

  public static void main(String[] args) {
    SpringApplication.run(BackendApplication.class, args);
  }
}
