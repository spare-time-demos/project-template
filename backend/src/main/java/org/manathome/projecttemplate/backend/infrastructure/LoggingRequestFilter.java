package org.manathome.projecttemplate.backend.infrastructure;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * request filter, adding logging information. 
 * 
 * * logs request and response data with "trace" level.
 * * adds a "session" MDC to logging, output into logs via %X{session} pattern.
 *  
 * @author man@home
 */
@Component
@Order(1)
public class LoggingRequestFilter implements Filter {

  static final Logger logger = LoggerFactory.getLogger(LoggingRequestFilter.class);

  static final String MDC_SESSION = "session";
  
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest req = (HttpServletRequest) request;
    HttpServletResponse res = (HttpServletResponse) response;
    HttpSession session = req.getSession(false);
    
    // TODO: pass a custom session/user identifier into mdc, allowing per session analysis of logs later. 
    MDC.put(MDC_SESSION, session != null ? session.getId() : "");
    logger.trace("Request  {} : {}", req.getMethod(), req.getRequestURI());

    chain.doFilter(request, response);

    logger.trace("Response :{}", res.getContentType());
    MDC.remove(MDC_SESSION);
  }

  // other methods
}
